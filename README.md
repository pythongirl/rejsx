# Rejsx
This script allows you to use compiled JSX in the browser. The JSX can either be
compiled on the server with Babel or on the client with [Babel standalone]
(https://www.npmjs.com/package/@babel/standalone). The JSX can be used as normal
DOM elements created by `document.createElement`.

## How to use in the browser
Just include rejsx and babel standalone like so:
```html
<script src="https://cdn.jsdelivr.net/npm/rejsx@1.1.1/main.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@babel/standalone@7.1.0/babel.min.js"></script>
```

Now you can simply use JSX in a script with type `text/babel`. Use the JSX as if you wre creating a DOM element with `document.createElement`.

```jsx
document.body.appendChild(<h1>Hello World</h1>)
```